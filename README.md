# Guide d'Utilisation de TimeSheet
Version 4 

## Téléchargement et Installation
1. Téléchargez l'application TimeSheet à partir du lien de téléchargement fourni.

2. Décompressez le fichier téléchargé dans un emplacement de votre choix sur votre ordinateur.

3. Une fois décompressé, accédez au dossier TimeSheet et localisez le fichier exécutable (.exe).

4. Pour un accès facile, créez un raccourci de l'exécutable en faisant un clic droit dessus et en sélectionnant "Créer un raccourci". Vous pouvez ensuite renommer le raccourci "TimeSheet" et le placer sur votre bureau ou dans un dossier accessible.

5. Si vous déplacez le dossier après avoir généré le raccourci, celui ci ne fonctionnera plus et il faudra le recréer.

## Utilisation de l'Application
1. Double-cliquez sur l'exécutable de TimeSheet pour démarrer l'application. Cela ouvrira automatiquement l'application dans votre navigateur web par défaut.

2. L'application TimeSheet s'exécute en arrière-plan, prête à enregistrer votre temps de travail sur différents projets.

3. Lorsque vous travaillez sur un projet spécifique, cliquez sur le bouton correspondant dans l'interface de l'application.

4. Pour ajouter un nouveau projet, saisissez son nom dans le champ "New project", puis cliquez sur le bouton "add" pour le valider.

5. Pour supprimer un projet inutile, sélectionnez-le dans le menu déroulant "Project to remove", puis cliquez sur le bouton "remove" pour le supprimer.

6. Une fois que vous avez terminé de travailler sur un projet ou que vous faites une pause, appuyez sur le bouton "Break".

7. Les heures que vous avez enregistrées apparaîtront dans un tableau, organisées par semaine. Vous pouvez copier ces valeurs dans la plateforme Odoo pour le suivi du temps.

8. Une fois que vous avez terminé d'enregistrer vos heures pour une semaine donnée, cliquez sur le bouton "clean this week" pour supprimer les données relatives à cette semaine. Les données sont stockées dans le fichier data/log_cleaned.txt.

9. Lorsque vous avez terminé votre journée de travail, cliquer sur le bouton "Stop program". Cela arrêtera l'exécution de l'application.

10. Si vous souhaitez corriger un horaire, accédez au fichier data/log.txt